require('./bootstrap')

import Vue from 'vue'

var app = new Vue({
  el: '#app',
  data: {
    message: 'Vue is ready!'
  }
})